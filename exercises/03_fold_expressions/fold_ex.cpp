#include <iostream>
#include <memory>
#include <set>
#include <string>
#include <string_view>
#include <vector>
#include <algorithm>

#include "catch.hpp"

using namespace std;

/////////////////////////////////////////////////////////////////////////////////////////////////

template <typename Container, typename... Args>
auto matches(const Container& c, const Args&... args)
{
    return (... + count(begin(c), end(c), args));
}

TEST_CASE("matches - returns how many items is stored in a container")
{
    vector<int> v{1, 2, 3, 4, 5, 3};

    REQUIRE(matches(v, 2, 5) == 2);
    REQUIRE(matches(v, 1, 3) == 3);
    REQUIRE(matches(v, 100, 200) == 0);
    REQUIRE(matches("abcdef", 'x', 'y', 'z') == 0);
    REQUIRE(matches("abcdef", 'a', 'd', 'f') == 3);
}

/////////////////////////////////////////////////////////////////////////////////////////////////

class Gadget
{
public:
    virtual std::string id() const { return "a"; }
    virtual ~Gadget() = default;
};

class SuperGadget : public Gadget
{
public:
    std::string id() const override
    {
        return "b";
    }
};

template <typename... Ts, typename ElementType = std::common_type_t<Ts...>>
auto make_vector(Ts&&... args)
{
    std::vector<ElementType> vec;
    vec.reserve(sizeof...(args));

    (vec.push_back(std::forward<Ts>(args)), ...);

    return vec;
}

TEST_CASE("make_vector - create vector from a list of arguments")
{
    using namespace Catch::Matchers;

    SECTION("ints")
    {
        std::vector<int> v = make_vector(1, 2, 3);

        REQUIRE_THAT(v, Equals(vector{1, 2, 3}));
    }

    SECTION("unique_ptrs")
    {
        auto ptrs = make_vector(make_unique<int>(5), make_unique<int>(6));

        REQUIRE(ptrs.size() == 2);
    }

    SECTION("unique_ptrs with polymorphic hierarchy")
    {
        auto gadgets = make_vector(make_unique<Gadget>(), make_unique<SuperGadget>(), make_unique<Gadget>());

        static_assert(is_same_v<decltype(gadgets)::value_type, unique_ptr<Gadget>>);

        vector<string> ids;
        transform(begin(gadgets), end(gadgets), back_inserter(ids), [](auto& ptr) { return ptr->id(); });

        REQUIRE_THAT(ids, Equals(vector<string>{"a", "b", "a"}));
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
struct Range : pair<T, T>
{
    Range(T first, T last) : pair<T, T>{std::move(first), std::move(last)}
    {}
};

template <typename T1, typename T2>
Range(T1, T2) -> Range<common_type_t<T1, T2>>;

template <typename T, typename... Ts>
bool within(const Range<T>& range, const Ts&... value)
{
    auto is_within = [&range](int v) { return v >= range.first && v <= range.second; };

    return (... && is_within(value));
}


TEST_CASE("within - checks if all values fit in range [low, high]")
{
    REQUIRE(within(Range{10, 20.0}, 1, 15, 30) == false);
    REQUIRE(within(Range{10, 20}, 11, 12, 13) == true);
    REQUIRE(within(Range{5.0, 5.5}, 5.1, 5.2, 5.3) == true);
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
void hash_combine(size_t& seed, const T& value)
{
    seed ^= hash<T>{}(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

template <typename... Ts>
size_t combined_hash(const Ts&... value)
{
    size_t seed = 0;
    (..., hash_combine(seed, value));

    return seed;
}

TEST_CASE("combined_hash - write a function that calculates combined hash value for a given number of arguments")
{
    size_t seed{};

    REQUIRE(combined_hash(1U) == 2654435770U);
    REQUIRE(combined_hash(1, 3.14, "string"s) == 10365827363824479057U);
    REQUIRE(combined_hash(123L, "abc"sv, 234, 3.14f) == 162170636579575197U);
}
