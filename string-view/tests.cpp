#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <string_view>

#include "catch.hpp"

using namespace std;

template <typename Container>
void print_items(const Container& c, string_view prefix = "")
{
    cout << prefix << " ";
    for(const auto& item : c)
        cout << item << " ";
    cout << '\n';
}


TEST_CASE("test")
{
    vector vec = { 1, 2, 3, 4 };

    print_items(vec, "items: ");

    string prefix = "Items:";

    print_items(vec, prefix);

    print_items(vec, "vec: "s);
}

string get_text()
{
    return "abc";
}


TEST_CASE("BEWARE")
{
    string lv = get_text();
    string_view sv_ok = lv;


    string_view sv_bad = get_text(); // dangling pointer
}
