#include <algorithm>
#include <iostream>
#include <map>
#include <numeric>
#include <set>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        vector<int>::const_iterator min_pos, max_pos;
        tie(min_pos, max_pos) = std::minmax_element(begin(data), end(data));

        double avg = accumulate(begin(data), end(data), 0.0) / data.size();

        return make_tuple(*min_pos, *max_pos, avg);
    }
} // namespace BeforeCpp17

template <typename Container>
tuple<int, int, double> calc_stats(const Container& data)
{
    auto [min_pos, max_pos] = std::minmax_element(begin(data), end(data));

    double avg = accumulate(begin(data), end(data), 0.0) / size(data);

    return tuple(*min_pos, *max_pos, avg);
}

struct Date
{
    int y, d;
    std::string m;
};

TEST_CASE("Structured Bindings")
{
    vector data = {3, 2, 1, 4, 5};

    SECTION("using tuples before C++17")
    {
        auto result = BeforeCpp17::calc_stats(data);

        const auto& min = get<0>(result);
        const auto& max = get<1>(result);
        const auto& avg = get<2>(result);

        REQUIRE(min == 1);
        REQUIRE(max == 5);
        REQUIRE(avg == Approx(3.0));
    }

    SECTION("using structured bindings")
    {
        int data2[] = {3, 2, 1, 4, 5};

        const auto [min, max, avg] = calc_stats(data2);

        REQUIRE(min == 1);
        REQUIRE(max == 5);
        REQUIRE(avg == Approx(3.0));
    }

    SECTION("allow unpack struct")
    {
        Date d1{2018, 7, "nov"};

        SECTION("by copy")
        {
            auto [year, day, month] = d1;

            d1.y = 2019;

            REQUIRE(year == 2018);
        }

        SECTION("by ref")
        {
            auto& [year, day, month] = d1;

            d1.y = 2019;
            day = 2;

            REQUIRE(d1.y == year);
            REQUIRE(d1.d == 2);
        }
    }

    SECTION("allow unpack items of array")
    {
        auto get_data = []() -> int(&)[3]
        {
            static int coords[3] = {1, 2, 3};
            return coords;
        };

        alignas(16) auto [x, y, z] = get_data();

        REQUIRE(x == 1);
        REQUIRE(y == 2);
        REQUIRE(z == 3);
    }
}

TEST_CASE("Use case 1")
{
    map<int, string> dict = {{1, "one"}, {2, "two"}, {3, "three"}};

    for (const auto& [key, value] : dict)
    {
        cout << key << " - " << value << endl;
    }
}

TEST_CASE("Use case 2")
{
    set<int> s = {1, 2, 4};

    auto [pos, was_performed] = s.insert(3);

    if (!was_performed)
        cout << "insert was not performed" << endl;
    else
        cout << *pos << " was inserted" << endl;
}

/////////////////////////////////////////////////////////////
/// tuple protocol for structure binding
///

enum Something
{
};

template <>
struct std::tuple_size<Something>
{
    static constexpr size_t value = 2;
};

template <>
struct std::tuple_element<0, Something>
{
    using type = int;
};

template <>
struct std::tuple_element<1, Something>
{
    using type = std::string;
};

//template <>
//auto get<0>(const Something&) { return 42; }

//template <>
//auto get<1>(const Something&) { return "text"s; }

// specializations seen above can be replaced with constexpr if implementation

template <size_t N>
auto get(const Something&)
{
    if constexpr (N == 0)
        return 42;
    else
        return "text"s;
}



TEST_CASE("structure binding with protocol")
{
    auto [some, thing] = Something{};

    REQUIRE(some == 42);
    REQUIRE(thing == "text"s);
}

///////////////////////////////////////////
///// BEWARE
/////

struct X
{
    int value;

    X(int value) : value{value}
    {
        cout << "X(" << value << ": " << this << ")\n";
    }

    X(const X& source) : value{source.value}
    {
        cout << "X(" << value << ": const X&: " << &source << " -> " << this << ")\n";
    }

    X(X&& source) : value{source.value}
    {
        cout << "X(X&&: " << &source << " -> " << this << ")\n";
    }

    ~X()
    {
        cout << "~X(" << value << ": " << this << ")\n";
    }
};

decltype(auto) f()
{
    auto [fst, snd] = pair("abbbbbbbbbbbbbbbbbbbbbbbbbbbbbc"s, X{8});

    return snd;
}

TEST_CASE("Buggy code")
{
    auto result = f();

    cout << "after f!!!" << endl;

    cout << &result << ": " << result.value << endl;
}
