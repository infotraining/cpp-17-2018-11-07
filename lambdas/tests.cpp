#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <functional>
#include <experimental/type_traits>

#include "catch.hpp"

using namespace std;

struct Gadget
{
    string id = "unknown";

    explicit Gadget(string id) : id{move(id)}
    {
        cout << "Gadget(" << this->id << " : " << this <<  ")\n";
    }

    ~Gadget()
    {
        cout << "~Gadget(" << id << " : " << this << ")\n";
    }

    Gadget(const Gadget& s) : id{s.id}
    {
        cout << "Gadget(cc: " << id << " : " << this << ")\n";
    }

    auto report()
    {
        return [*this] {
            cout << "Gadget(" << id << " : " << this << ")\n";
            do_stuff();
        };
    }

private:
    void do_stuff() const
    {
        cout << "Gadget::do_stuff" << endl;
    }
};

TEST_CASE("test")
{
    function<void()> reporter;

    {
        Gadget g{"smartphone"};

        reporter = g.report();
    }

    reporter();
}

TEST_CASE("constexpr lambdas")
{
    auto l1 = [](int x) { return x * x; };

    static_assert (l1(4) == 16);

    int arr[l1(8)] = {};

    static_assert(size(arr) == 64);
}


namespace Experimental
{
    template <typename Iter, typename Pred>
    constexpr Iter find_if(Iter first, Iter last, Pred pred)
    {
        for(Iter it = first; it != last; ++it)
            if (pred(*it))
                return it;
        return last;
    }
}

TEST_CASE("constexpr find_if")
{
    constexpr array<int, 8> arr = { 1, 2, 4, 16, 32, 64, 665, 1024 };

    constexpr auto size_of_array = *Experimental::find_if(begin(arr), end(arr), [](int x) { return x % 5 == 0; });

    static_assert (size_of_array == 665);
}

//////////////////////////////////////////
/// Detection Idiom
///

struct X
{
    X& operator=(const X&) = delete;
};

template <typename T>
using check_if_copy_assignable = decltype( declval<X>() = declval<X>() );

TEST_CASE("is copy assignable")
{
    X x1;

    //static_assert( x1 = X{} );

    static_assert(!std::experimental::is_detected_v<check_if_copy_assignable, X>);
}

/////////////////////////////
//

void foo(std::unique_ptr<X> ptr, int arg)
{
    //...
}

int may_throw()
{
    // throw 42;

    return 665;
}

TEST_CASE("mem leak in C++14")
{
    foo(std::unique_ptr<X>(new X{}), may_throw());
}
