#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
void foo1(T item)
{
    puts(__PRETTY_FUNCTION__);
}

void test() {}

template <typename T>
void foo2(T& item)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void foo3(T&& item)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("Type deduction")
{
    int x = 10;
    int& ref_x = x;
    const int& cref_x = x;

    SECTION("Case1 - type deduction")
    {
        auto ax1 = 1; // int
        auto ax2 = x; // int
        auto ax3 = ref_x; // int
        auto ax4 = cref_x; // int

        foo1(1);
        foo1(x);
        foo1(ref_x);
        foo1(cref_x);

        int tab[10];

        // array decays to pointer
        auto atab = tab; // int(*)[10]
        foo1(atab);

        // decay to function pointer
        auto af = test; // void(*)()
        foo1(test);
    }

    cout << "\n\n";

    SECTION("Case2 - type deduction")
    {
        //auto& ax1 = 1; // int&
        auto& ax2 = x; // int&
        auto& ax3 = ref_x; // int&
        auto& ax4 = cref_x; // const int&

        //foo2(1);
        foo2(x);
        foo2(ref_x);
        foo2(cref_x);

        int tab[10];

        // no decay
        auto& atab = tab; // int(&)[10]
        foo2(atab);

        // decay to function pointer
        auto& af = test; // void(&)()
        foo2(test);

        foo2("test");
    }

    cout << "\n\n";

    SECTION("Case3 - type deduction")
    {
        auto&& ax1 = 1; // int&&
        auto&& ax2 = x; // int&

        foo3(1);
        foo3(x);
    }
}

////////////////////////////////////////////
///
///


template <typename T1, typename T2>
struct Data
{
    T1 fst;
    T2 snd;

    Data(T1 f, T2 s) : fst{f}, snd{s}
    {}

    using type1 = T1;
    using type2 = T2;
};

TEST_CASE("class template arg deduction")
{
    Data<int, string> d1{1, "text"};

    Data d2(1, "text");

    static_assert(is_same_v<decltype(d2)::type1, int>);
    static_assert(is_same_v<decltype(d2)::type2, const char*>);
}

//////////////////////////////////////////////////////
///
///

template <typename T1, typename T2>
struct Pair
{
    T1 fst;
    T2 snd;

    Pair(const T1& f, const T2& s) : fst{f}, snd{s}
    {}

    using type1 = T1;
    using type2 = T2;
};

// deduction guide
template <typename T1, typename T2>
Pair(T1, T2) -> Pair<T1, T2>;

TEST_CASE("deduction with ref")
{
    const int x = 10;

    Pair p1{x, "test"};

    static_assert(is_same_v<decltype(p1)::type1, int>);
    static_assert(is_same_v<decltype(p1)::type2, const char*>);
}

//////////////////////////////////////////////
/// Overloading deduction guides

template <typename T>
struct Container
{
    T items;

    Container(const T& i) : items{i}
    {
    }

    template <typename IT>
    Container(initializer_list<IT> il) : items{il}
    {
    }
};

template <typename T>
Container(T) -> Container<T>;

template <typename T>
Container(initializer_list<T>) -> Container<vector<T>>;

Container(const char*) -> Container<string>;

TEST_CASE("using Container")
{
    Container c1(1); // Container<int>

    Container c2{1, 2, 3, 4};

    Container c3("text"); // Container<string>
}

///////////////////////////////////////////////
/// How to disable type deduction
///
///


template <typename T>
struct SmartPointer
{
    using T_ = common_type_t<T>;

    T_* ptr;

    SmartPointer(T_* ptr) : ptr{ptr}
    {}

    ~SmartPointer()
    {
        delete ptr;
    }

    T* get() const
    {
        return ptr;
    }

    SmartPointer(const SmartPointer&) = delete;
    SmartPointer& operator=(const SmartPointer&) = delete;
};

TEST_CASE("Disable type deduction")
{
    int x{};
    SmartPointer<int> ptr = &x;
}

template <typename T>
struct Wrapper
{
    T item;

    Wrapper(const T& item) : item{item}
    {}
};

template <typename T>
Wrapper(T) -> Wrapper<T>;

TEST_CASE("Constructor vs deduction guide")
{
    Wrapper w = 6;

    Wrapper cstr = "test";
}

//////////////////////////////////////
///
///

template <auto N>
struct my_integral_constant
{
    constexpr static auto value = N;
};

TEST_CASE("integral constant")
{
    static_assert(my_integral_constant<4>::value == 4);
}
