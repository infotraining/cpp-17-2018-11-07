#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Response
{
    string msg;
    int error_code;
};

[[nodiscard]] Response foo()
{
    return { "ok", 0 };
}

TEST_CASE("test")
{
    if (auto [msg, errc] = foo(); errc == 0)
    {
        cout << "ok" << endl;
    }

    [[maybe_unused]] int x = 9;
}

