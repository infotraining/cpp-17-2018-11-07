#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <complex>
#include <array>

#include "catch.hpp"

using namespace std;

struct CData
{
    int a;
    double b;
    int tab[3];

    void print() const
    {
        cout << a << " " << b << " [";
        for(const auto& item : tab)
            cout << item << " ";
        cout << "]\n";
    }
};

// inheritence from aggregate
struct Derived : CData
{
    string str;
};

TEST_CASE("test")
{
    int arr[3] = { 1, 2, 3 };
    CData c1{ 1, 3.14, {1, 2, 3} };
    c1.print();

    Derived d{ {1, 3.14, {} }, "text" };

    REQUIRE(d.a == 1);
    REQUIRE(d.b == 3.14);
    REQUIRE(d.tab[0] == 0);
    REQUIRE(d.str == "text"s);
}

template <typename T>
struct ComplexAggregate : std::string, std::complex<T>
{
    string d;
};

TEST_CASE("ComplexAggregate")
{
    ComplexAggregate<double> agg { { "txt" }, {4.14, 3.2}, "abc" };
}

TEST_CASE("std::array")
{
    std::array<int, 10> arr = { { 1, 2, 3, 4 } };
}

struct Strange
{
    Strange() = delete;

    int x = 4;
};

TEST_CASE("WTF")
{
    static_assert(is_aggregate_v<Strange>);

    Strange s{};

    REQUIRE(s.x == 4);
}
