#include <algorithm>
#include <any>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
    std::any obj = 4;
    obj = 3.14;
    obj = "text"s;

    cout << "sizeof: " << sizeof(obj) << endl;

    cout << obj.type().name() << endl;

    SECTION("any_cast<T>(any&) - returns copy")
    {
        REQUIRE(any_cast<string>(obj) == "text"s);
        REQUIRE_THROWS_AS(any_cast<double>(obj), std::bad_any_cast);
    }

    SECTION("any_cast<T>(any*) - returns pointer")
    {
        string* ptr = any_cast<string>(&obj);
        REQUIRE(ptr != nullptr);

        *ptr += "abc";

        REQUIRE(any_cast<int>(&obj) == nullptr);
    }
}
