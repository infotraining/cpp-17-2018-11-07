cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)

get_filename_component(PROJECT_NAME_STR ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${PROJECT_NAME_STR})

cmake_minimum_required(VERSION 2.8)
project(${PROJECT_NAME_STR})

set(CMAKE_CXX_STANDARD 17)

set(LIBS_TBB_DIR "./libs/tbb")
set(LIBS_PSTL_DIR "./libs/parallelstl")

include(${LIBS_TBB_DIR}/cmake/TBBBuild.cmake)
include(${LIBS_TBB_DIR}/cmake/TBBGet.cmake)

# Download TBB
tbb_get(TBB_ROOT tbb_root SOURCE_CODE)

# Build Intel TBB with enabled Community Preview Features (CPF).
tbb_build(TBB_ROOT ${tbb_root} CONFIG_DIR TBB_DIR MAKE_ARGS tbb_cpf=1)

find_package(TBB REQUIRED tbb_preview)

#----------------------------------------
# Application
#----------------------------------------
add_definitions("-fopenmp")

include_directories(${tbb_root}/inlude ${LIBS_PSTL_DIR}/include)

aux_source_directory(. SRC_LIST)

# Headers
file(GLOB HEADERS_LIST "*.h" "*.hpp")
add_executable(${PROJECT_NAME} ${SRC_LIST} ${HEADERS_LIST})

# Link Intel TBB imported targets to the executable;
# "TBB::tbb_preview" can be used instead of "${TBB_IMPORTED_TARGETS}".
target_link_libraries(${PROJECT_NAME} ${TBB_IMPORTED_TARGETS} stdc++fs)

file(COPY proust.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR})