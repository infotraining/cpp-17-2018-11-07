#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

enum DayOfWeek : uint8_t { mon, tue, wed, thd, fri, sat, sun };

TEST_CASE("enums and list intialization")
{
    DayOfWeek d1 = wed;

    DayOfWeek d2{4};
}

enum class Index : size_t
{};

size_t to_uint(Index i)
{
    return static_cast<size_t>(i);
}

struct Container
{
    vector<int> data;

    int& at(Index n)
    {
        auto temp = n * 2;
        return data[to_uint(n)];
    }
};

TEST_CASE("use case for new enums")
{
    Container c { {1, 2, 3} };

    REQUIRE(c.at(Index{1}));
}


////////////////////////////////////////////////////////////
//

TEST_CASE("list init with auto")
{
    int x1{1};
    int x2 = 2;
    int x3 = {3};

    auto ax1{1};  // C++17 - int
    //auto ax1a{1, 2, 3};  // ill-formed
    auto ax1b(1);  // C++14 - int
    auto ax2 = 2; // C++14 - int
    auto ax3 = {3}; // C++14 - initializer_list<int>
}
