#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{

   template <typename T>
   auto sum(T arg)
   {
       return arg;
   }

   template <typename Head, typename... Tail>
   auto sum(Head head, Tail... tail)
   {
       return head + sum(tail...);
   }
}


// left fold
template <typename... Args>
auto sum(Args... args)
{
    return (... + args);
}

// right fold
template <typename... Args>
auto sum_r(Args... args)
{
    return (args + ...);
}

template <typename Head, typename... Args>
void print_all(const Head& head, const Args&... args)
{
    auto spaced = [](const auto& arg) {
        cout << " ";
        return arg;
    };

    std::cout << head;

    (std::cout << ... << spaced(args)) << '\n';
}

template <typename... Args>
void print_all_alt(const Args&... args)
{
    // alt take
    ((std::cout << args << " "), ...);
}

TEST_CASE("test")
{
    auto result_left = sum("1"s, "2"s, "3"s, "4"s, "5"s, "6"s);

    REQUIRE(result_left == "123456"s);

    auto result_right = sum_r("1"s, "2"s, "3"s, "4"s, "5"s, "6"s);

    REQUIRE(result_right == "123456"s);

    print_all("1"s, "2"s, "3"s, "4"s, "5"s, "6"s);
    print_all_alt("1"s, "2"s, "3"s, "4"s, "5"s, "6"s);
}
