# README #

### Docs ###

* https://infotraining.bitbucket.io/cpp-17/

### Pre-test ###

* https://goo.gl/forms/n4c77dgoOTo1K2sI3

### Post-test ###

* https://goo.gl/forms/kaLGJJpf4Gq0StY83

### Ankieta ###

* https://www.infotraining.pl/ankieta/cpp-17-2018-11-07-kp

### Proxy settings ###

* add to .profile

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```
