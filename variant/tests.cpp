#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <variant>
#include <vector>

#include "catch.hpp"

using namespace std;

struct PrintVisitor
{
    void operator()(int x) const
    {
        cout << "int(" << x << ")\n";
    }

    void operator()(double d) const
    {
        cout << "double(" << d << ")\n";
    }

    void operator()(const string& str) const
    {
        cout << "string(" << str << ")\n";
    }
};

TEST_CASE("test")
{
    variant<int, double, string> v;

    v = 3.14;

    visit(PrintVisitor{}, v);
}

template <typename... Vs>
struct Overloader : Vs...
{
    using Vs::operator()...;
};

template <typename... Ts>
Overloader(Ts...)->Overloader<Ts...>;

TEST_CASE("inline visitor")
{
    auto inline_visitor = Overloader{
        [](int x) { cout << "int(" << x << ")\n"; },
        [](double d) { cout << "double(" << d << ")\n"; },
        [](const string& s) { cout << "str(" << s << ")\n"; }
    };

    variant<int, double, string> v;

    v = "text"s;

    visit(inline_visitor, v);
}
