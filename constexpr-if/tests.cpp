#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

//////////////////////////////////////////////////////////////
// variadic templates
//

namespace BeforeCpp17
{
    void print()
    {
        cout << "\n";
    }

    template <typename Head, typename... Tail>
    void print(Head head, Tail... tail)
    {
        cout << head << " ";
        print(tail...);
    }
}

template <typename Head, typename... Tail>
void print(Head head, Tail... tail)
{
    cout << head << " ";

    if constexpr (sizeof...(tail) > 0)
        print(tail...);
    else
        cout << "\n";
}

TEST_CASE("variadic templates")
{
    print(1, "text"s, 3.14);
}
