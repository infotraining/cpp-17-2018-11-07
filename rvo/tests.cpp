#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

std::vector<int> create_vector()
{
    return vector<int>{ 1, 2, 3, 4, 5, 6 };
}

TEST_CASE("rvo")
{
    auto vec = create_vector();
}

struct NoCopyAndMove
{
    int value;
    NoCopyAndMove(int x) : value{x} {}
    NoCopyAndMove(const NoCopyAndMove&) = delete;
    NoCopyAndMove& operator=(const NoCopyAndMove&) = delete;
    NoCopyAndMove(NoCopyAndMove&&) = delete;
    NoCopyAndMove& operator=(NoCopyAndMove&&) = delete;
};

struct OnlyMoveable
{
    int value;
    OnlyMoveable(int x) : value{x} {}
    OnlyMoveable(const OnlyMoveable&) = delete;
    OnlyMoveable& operator=(const OnlyMoveable&) = delete;
    OnlyMoveable(OnlyMoveable&&) = default;
    OnlyMoveable& operator=(OnlyMoveable&&) = default;
};


NoCopyAndMove create_ncm_rvo()
{
    return NoCopyAndMove{42};
}

OnlyMoveable create_om()
{
    return OnlyMoveable{43};
}

//NoCopyAndMove create_ncm_nrvo()
//{
//    NoCopyAndMove result{42};
//    // ...
//    return result;
//}


TEST_CASE("NoCopyAndMove")
{
    auto ncm = create_ncm_rvo();
}

TEST_CASE("object materialization")
{
    NoCopyAndMove&& refref = NoCopyAndMove{1};

    REQUIRE(NoCopyAndMove{6}.value == 6);
}


// copy elision for arguments
void foo(NoCopyAndMove param)
{
    cout << param.value << endl;
}

TEST_CASE("copy elision for arguments")
{
    foo(NoCopyAndMove{665});
}



struct X
{
    std::string value;

    explicit X(string v) : value{std::move(v)}
    {}
};


TEST_CASE("passing strings as arguments")
{
    X x1{"test"s};

    string lvalue_str = "text";
    X x2{lvalue_str};

    X x3{std::move(lvalue_str)};
}
