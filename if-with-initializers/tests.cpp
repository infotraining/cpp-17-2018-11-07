#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <queue>

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initializers")
{
    vector<int> vec = { 1, 2, 3, 4, 5 };

    if (auto it = find(begin(vec), end(vec), 3); it != end(vec))
    {
        cout << "Item " << *it << " has been found..." << endl;
    }
}

TEST_CASE("use with mutex")
{
    queue<int> q;
    mutex q_mtx;

    SECTION("Before Cpp17")
    {
        {
            lock_guard<std::mutex> lk{q_mtx};

            if (!q.empty())
            {
                int value = q.front();
                q.pop();
            }
        }

        //...
    }

    SECTION("C++17")
    {
        if(lock_guard lk{q_mtx}; !q.empty())
        {
            int value = q.front();
            q.pop();
        }

        //...
    }
}
